ARG CONSUL_VERSION=${CONSUL_VERSION:-1.8}
FROM consul:${CONSUL_VERSION}
LABEL maintainer="Dusan Simek <pixelfields@runbox.com>"

RUN apk add --no-cache \
        python3 \
        py3-pip \
    && pip3 install --upgrade pip \
    && pip3 install \
        awscli \
    && rm -rf /var/cache/apk/*

RUN aws --version
